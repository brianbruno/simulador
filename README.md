# Simulador de Empréstimo

Certifique-se que tenha composer e php instalado no ambiente.

Para executar:

```
composer install
php artisan serve
```

#### Teste:

Para testar basta importar a collection "Simulador.postman_collection.json" no Postman.


Exitem 3 rotas:

```
1. api/instituicoes
2. api/convenios
3. api/simular
```

Rota 1: Responsável por buscar todas as instituições cadastradas

Rota 2: Responsável por buscar todos os convênios cadastrados

Rota 3: Simula os empréstimos


#### Detalhes:

1. O arquivo .env foi commitado para fins de facilidade ao executar o sistema
2. Considerei o campo "parcela" na rota para simulação como o número de parcelas desejadas
