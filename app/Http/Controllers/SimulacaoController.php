<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class SimulacaoController
{
    public function simular(Request $request)
    {

        $retorno = array(
            'status' => true,
            'simulacoes' => []
        );

        try {
            $validator = Validator::make($request->all(), [
                'valor_emprestimo' => 'required:numeric'
            ]);

            // Eu preferi não utilizar o validator para todos os campos, mesmo sabendo que eu poderia
            // Acredito que assim fica mais prático para resolver possíveis problemas
            if ($validator->fails()) {
                throw new \Exception('O campo valor_emprestimo é obrigatório.');
            }

            $valorSolicitado = $request->get('valor_emprestimo');
            $instituicoes = $request->get('instituicoes');
            $convenios = $request->get('convenios');
            $parcela = $request->get('parcela');

            if (!empty($instituicoes) and !is_array($instituicoes))
                throw new \Exception('O campo instituicoes precisa ser do tipo array.');
            elseif (empty($instituicoes))
                $instituicoes = json_decode(Storage::get('instituicoes.json'), true);

            if (!empty($convenios) and !is_array($convenios))
                throw new \Exception('O campo convenios precisa ser do tipo array.');
            elseif (empty($convenios))
                $convenios = json_decode(Storage::get('convenios.json'), true);

            if (!empty($parcela) and !is_numeric($parcela))
                throw new \Exception('O campo parcela precisa ser do tipo numérico.');
            elseif(empty($parcela))
                $parcela = -1;

            foreach($instituicoes as $instituicao) {
                $taxas = $this->getTaxasSolicitadas($instituicao['chave'], $convenios, $parcela);

                foreach ($taxas as $taxa) {

                    if (empty($retorno['simulacoes'][$instituicao['chave']])) {
                        $retorno['simulacoes'][$instituicao['chave']] = [];
                    }
                    $retorno['simulacoes'][$instituicao['chave']][] = array(
                        'taxa'     => $taxa['taxaJuros'],
                        'parcelas' => $taxa['parcelas'],
                        'valor_parcela' => round($valorSolicitado * $taxa['coeficiente'], 2),
                        'convenio' => $taxa['convenio'],
                    );
                }


            }




        } catch(\Exception $e) {
            $retorno['status'] = false;
            $retorno['mensagem'] = $e->getMessage();
        }

        return response()->json($retorno);

    }

    // Simulando uma função que consultaria em um Model (banco de dados)
    private function getTaxasSolicitadas($nome, $convenios, $parcela)
    {
        $taxasInstituicoes = Storage::get('taxas_instituicoes.json');
        $taxasInstituicoes = json_decode($taxasInstituicoes, true);

        $resultado = [];
        $arrayConvenios = [];

        foreach($convenios as $convenio) {
            $arrayConvenios[] = $convenio['chave'];
        }

        foreach ($taxasInstituicoes as $taxa) {
            // Verifica se é da instituição solicitada (caso não tenha solicitado, considera todas)
            // Verifica se é do convênio solicitado (caso não tenha solicitado, considera todos)
            // Verifica se é da parcela solicitada (caso tenha solicitado)
            if ($taxa['instituicao'] === $nome and in_array($taxa['convenio'], $arrayConvenios, false) and ($parcela === -1 or $parcela === $taxa['parcelas'])) {
                $resultado[] = $taxa;
            }
        }

        if (count($resultado) === 0)
            throw new \Exception('Nenhuma oferta de emprestimo disponível.');

        return $resultado;
    }

}
