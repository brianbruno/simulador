<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ConvenioController
{
    public function getConvenios(Request $request)
    {

        $retorno = array(
            'status' => true,
            'convenios' => []
        );

        try {
            $convenios = Storage::get('convenios.json');

            $retorno['convenios'] = json_decode($convenios, true);
        } catch(\Exception $e) {
            $retorno['status'] = false;
            $retorno['mensagem'] = $e->getMessage();
        }

        return response()->json($retorno);

    }
}
