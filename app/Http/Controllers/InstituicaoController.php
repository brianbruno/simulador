<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class InstituicaoController {

    public function getInstituicoes(Request $request)
    {

        $retorno = array(
            'status' => true,
            'instituicoes' => []
        );

        try {
            $instituicoes = Storage::get('instituicoes.json');

            $retorno['instituicoes'] = json_decode($instituicoes, true);
        } catch(\Exception $e) {
            $retorno['status'] = false;
            $retorno['mensagem'] = $e->getMessage();
        }

        return response()->json($retorno);

    }

}
